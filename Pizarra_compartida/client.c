#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <gtk/gtk.h>
#include <pthread.h>
#include <signal.h>
#include "connect.h"



// color actual propio para intercambiar con el borrar
int color[3]={0,0,0};
int color_blanco[3]={0xffff,0xffff,0xffff};
//ancho de linea
int line_width = 5;

//mapa de pixeles que se dibujará
GdkPixmap *pixmap = NULL;
//crayon del usuario
GdkGC *pen = NULL;
//color del usuario
GdkColor *c = NULL;


//builder de gtk
GtkBuilder *b;

//WIDGETS
//ventana principal
GtkWidget *FrmPrincipal;
//area de dibujo
GtkWidget *drawing_area;
//boton de cambio de color
GtkWidget *BtnColor;
//boton de borrar del fondo
GtkWidget *BtnColorFondo;
//boton de borrar
GtkWidget *BtnBorrar;
//boton conectar
GtkWidget *BtnConectar;
//boton que maneja el ancho de la linea
GtkWidget *BtnSpin;

//punto anterior
gint point[2];

//para las conecciones del socket
int sockfd;
struct hostent* he; 
struct sockaddr_in addr; /* dirección del server donde se conectara */ 
char* dir;
int sockfd_point = -1;

//arreglo de miembros
struct point* array_miembro;//para almacenar los colores de los miembros (lo distingo de los puntos que son volatiles)

//puntos volatiles
struct point* last_point; //para almacenar los puntos de los miembros
struct point* prev_point; //punto anterior del miembro

//se utiliza para almacenar el punto a enviar
struct point mi_punto;

struct point me;

//hilo de ejecucion que escucha al servidor
pthread_t tid;


//arreglo de crayones de los miembros
GdkGC **array_pen; 

//arreglo de colores de los miembros
GdkColor **array_colors;

//flags
int draw = 0;
int draw_other[10];
int boolean = 0;
int borrar_clicked = 0;
int ini_coneccion = 0;
int conectado = 0;
int stop = 0;
int release = 0;
//crea y retorna un GdkGC* que simboliza a un crayon en la libreria
//debe recibir el mapa de pixeles, las componenetes de color y una instancia de GdkColor *
GdkGC *GetPen (GdkPixmap *pixmap, int nRed, int nGreen, int nBlue, GdkColor **color)
{
	GdkColor *c;
	GdkGC *gc;
	color[0] = (GdkColor *) g_malloc (sizeof (GdkColor));
    c = color[0];
	c->red = nRed;
	c->green = nGreen;
	c->blue = nBlue;
	gdk_color_alloc (gdk_colormap_get_system (), c);
	gc = gdk_gc_new (pixmap);
	gdk_gc_set_foreground (gc, c);
	return (gc);
}

//le setea el tamaño al crayon
void set_line_width(int w, GdkGC* pen)
{
	gdk_gc_set_line_attributes(pen,w,GDK_LINE_SOLID,
           GDK_CAP_BUTT, GDK_JOIN_ROUND);
}

//le setea el color al crayon por defecto del cliente
void set_line_color(int r, int g, int b)
{
	c->red = r;
	c->green = g;
	c->blue = b;
    gdk_color_alloc (gdk_colormap_get_system (), c);
	gdk_gc_set_foreground (pen, c);

}

//se utiliza para desconectarse del servidor finalizando el hilo que escucha al servidor
void desconectar()
{
	printf("El servidor esta caido: cerrando coneccion\n");
	stop = 1;
}


//avisa de un evento release por parte del miembro
void enviar_release()
{
	if(conectado == 1)
	{
		mi_punto.point[0] = RELEASE;
		mi_punto.point[1] = RELEASE; 

		if (send(sockfd_point, &mi_punto, sizeof(struct point), 0) == -1)
			desconectar();
		fflush(NULL);

	}
}

//envia un evento de movimiento y el punto actual por parte del miembro
void enviar_punto()
{
	//printf("ENVIAR %i %i\n",point[0],point[1]);

	if(conectado == 1)
	{
		if(point[0] > -1){
			mi_punto.point[0] = point[0];
			mi_punto.point[1] = point[1]; 

			if (send(sockfd_point, &mi_punto, sizeof(struct point), 0) == -1){
				desconectar();
			}
			fflush(NULL);
			release = 0;
		}
		else if(release == 0){
			enviar_release();
			release = 1;
		}

	}
}

//envia el nuevo color del miembro al servidor
void enviar_color()
{
	if(conectado == 1)
	{	
 		me.red = color[0];
		me.green = color[1];
		me.blue = color[2];

		me.point[0] = NEWCOLOR; //marco que envio un color
		me.line_width = line_width;
		//printf("Color a enviar: (%ld, %ld, %ld)\n",me.red,me.green,me.blue);

		if (send(sockfd_point, &me, sizeof(struct point), 0) == -1){
			printf("Error de coneccion\n");
			desconectar();
		}
		fflush(NULL);
	}

	
}

//setea el nuevo fondo al pixmap
void cambiar_color_fondo(int red, int green, int blue)
{
	GdkColor *c = (GdkColor *) g_malloc (sizeof (GdkColor));

	c->red = red;
	c->green = green;
	c->blue = blue;

    gdk_color_alloc (gdk_colormap_get_system (), c);

	GdkGC *gc;

	gdk_color_alloc (gdk_colormap_get_system (), c);
	gc = gdk_gc_new (pixmap);
	gdk_gc_set_foreground (gc, c);

	GdkRectangle update_rect;
		
	update_rect.x = 0 ;//- 5
	update_rect.y = 0 ;
	update_rect.width = 800;
	update_rect.height = 600;

	gdk_draw_rectangle (pixmap, gc, TRUE, 0, 0, 800,600);		

	gtk_widget_draw (drawing_area, &update_rect);

	color_blanco[0] = red;
	color_blanco[1] = green;
	color_blanco[2] = blue;	
}

//envia el cambio de color de fondo al servidor
void enviar_color_fondo(GdkColor *c)
{ 
	cambiar_color_fondo(c->red,c->green,c->blue);

	if(conectado == 1)
	{	
 		me.red = c->red;
		me.green = c->green;
		me.blue = c->blue;

		me.point[0] = NEWFONDO; //marco que envio un color
		me.line_width = line_width;
		//printf("Color a enviar: (%ld, %ld, %ld)\n",me.red,me.green,me.blue);

		if (send(sockfd_point, &me, sizeof(struct point), 0) == -1)
			desconectar();
		fflush(NULL);
	}

	
}


//envia el color borrar actual del miembro al servidor (depende del color del fondo)
void enviar_borrar()
{
	if(conectado == 1)
	{

		me.red = color_blanco[0];
		me.green = color_blanco[1];
		me.blue = color_blanco[2];		
	
		me.point[0] = NEWCOLOR; //marco que envio un color
		me.line_width = line_width;
		//printf("Color a enviar: (%ld, %ld, %ld)\n",me.red,me.green,me.blue);

		if (send(sockfd_point, &me, sizeof(struct point), 0) == -1)
			desconectar();
		fflush(NULL);
	}
}

//actualiza el crayon del miembro "id" con el color seteado anteriormente
void actualizar_pen(int id)
{
	
	array_colors[id]->red = array_miembro[id].red;
	array_colors[id]->green = array_miembro[id].green;
	
	array_colors[id]->blue = array_miembro[id].blue;
    gdk_color_alloc (gdk_colormap_get_system (), array_colors[id]);
	gdk_gc_set_foreground (array_pen[id], array_colors[id]);
	
	gdk_gc_set_line_attributes(array_pen[id],array_miembro[id].line_width,GDK_LINE_SOLID,
           			GDK_CAP_BUTT, GDK_JOIN_ROUND);


}

//evento que inicializa el pixmap
static gint configure_event (GtkWidget *widget, GdkEventConfigure *event)
{
	if (pixmap)
		gdk_pixmap_unref(pixmap);

	pixmap = gdk_pixmap_new(widget->window,
		                  widget->allocation.width,
		                  widget->allocation.height,
		                  -1);
	if(pen==NULL){
		pen = GetPen(pixmap,color[0],color[1],color[2],&c);

		set_line_width(5,pen);
		set_line_color(0,0,0);
	}

	gdk_draw_rectangle (pixmap,
		              widget->style->white_gc,
		              TRUE,
		              0, 0,
		              widget->allocation.width,
		              widget->allocation.height);

	return TRUE;
}

//evento que inicializa el drawing area con el pixmap
static gint expose_event (GtkWidget *widget, GdkEventExpose *event)
{
  gdk_draw_pixmap(widget->window,
                  widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
                  pixmap,
                  event->area.x, event->area.y,
                  event->area.x, event->area.y,
                  event->area.width, event->area.height);

  return FALSE;
}

//pinta la linea del miembro actual
void draw_brush(GtkWidget *widget, gdouble x, gdouble y)
{
	if(draw==1)
	{
		  
		GdkRectangle update_rect;
		
		update_rect.x = 0 ;//- 5
		update_rect.y = 0 ;
		update_rect.width = widget->allocation.width;
		update_rect.height = widget->allocation.height;

		gdk_draw_line(pixmap,pen,point[0],point[1],x,y);
		
		gtk_widget_draw (widget, &update_rect);
	}

	else draw = 1;
	
	point[0] = x;
	point[1] = y;
	//0 o 1
	
}

//pinta la linea entre el punto anterior y el siguiente del mismbro "id"
void draw_brush_other( int id)
{
	if(draw_other[id]==1)
	{
		  
		  GdkRectangle update_rect;
		
		  update_rect.x = 0 ;//- 5
		  update_rect.y = 0 ;
		  update_rect.width = drawing_area->allocation.width;
		  update_rect.height = drawing_area->allocation.height;
		
		  //if(prev_point[id].point[0]>0 && prev_point[id].point[1]>0 && last_point[id].point[0]>0 && last_point[id].point[1]>0 )
		  {
		  	//TODO
			//printf("IMPRIMIR %i %i %i %i\n",prev_point[id].point[0],prev_point[id].point[1],last_point[id].point[0],last_point[id].point[1]);
			
			gdk_draw_line(pixmap,array_pen[id],prev_point[id].point[0],prev_point[id].point[1],last_point[id].point[0],last_point[id].point[1]);
			

			gtk_widget_queue_draw_area(drawing_area,update_rect.x,update_rect.y,update_rect.width,update_rect.height);
		}
	}

	else draw_other[id] = 1;
	//else
	//0 o 1
	
}


//se ejecuta ante un evento de press e indica que debe comenzar a dibujarse ante los motion event
static gint button_press_event (GtkWidget *widget, GdkEventButton *event)
{
	

  	int x, y;
 	if (event->button == 1 && pixmap != NULL)
    //  draw_brush (widget, event->x, event->y);
	{
	  draw_brush (widget, event->x, event->y);
	  x = event->x;
	  y = event->y;
	  boolean = 1;
	}
	enviar_punto();

    return TRUE;
}

//se ejecuta ante un evento de release e indica que debe dejar de dibujarse ante un motion event
static gint button_release_event (GtkWidget *widget, GdkEventButton *event)
{

 	if (event->button == 1)// && pixmap != NULL)
    //  draw_brush (widget, event->x, event->y);
	{
	 
	  boolean = 0;
      draw = 0;
      enviar_release();
	}
  return TRUE;
}

//se ejecuta cuando hay movimiento sobre el drawing area solo si antes hubo un evento de pressed y no hubo despues ningun evento de release
static gint motion_notify_event (GtkWidget *widget, GdkEventMotion *event)
{
	if(	  boolean ==1)
	{

		int x, y;
		GdkModifierType state;

		if (event->is_hint)
		gdk_window_get_pointer (event->window, &x, &y, &state);
		else
		{
		  x = event->x;
		  y = event->y;
		  state = event->state;
		}

		if (state & GDK_BUTTON1_MASK && pixmap != NULL)
			draw_brush (widget, x, y);
		enviar_punto();
	}
	return TRUE;
}

//funcion que se ejecuta ante un cambio en el boton de eleccion de color
static gint color_change_event(GtkWidget *widget, GdkEvent *event)
{
	if(borrar_clicked == 0){
	    gtk_color_button_get_color((GtkColorButton*)widget, c);
	    gdk_color_alloc (gdk_colormap_get_system (), c);
	    color[0] = c->red;
	    color[1] = c->green;
	    color[2] = c->blue;
		gdk_gc_set_foreground (pen, c);
		enviar_color();
	}

}

//funcion que se ejecuta ante el evento de presionar el boton borrar
//configura el color de borrado o el color original segun sea el caso
static gint boton_borrar_clicked(GtkWidget *widget, GdkEvent *event)
{

	if(borrar_clicked == 0)//borrar presionado
	{
		set_line_color(color_blanco[0],color_blanco[1],color_blanco[2]);
		set_line_width(line_width,pen);
		borrar_clicked = 1;
		enviar_borrar();
	}
	else//borrar soltado
	{
		set_line_color(color[0],color[1],color[2]);
		set_line_width(line_width,pen);
		borrar_clicked = 0;		
		enviar_color();

	}
}

//funcion que ejecuta el hilo que se conecta con el servidor
void escuchar_por_puntos()
{
	struct point last;
	int id;
	if(borrar_clicked == 0)
	    enviar_color();
	else
		enviar_borrar();

	while(stop==0)
	{

		if (recv(sockfd_point, &last, sizeof(struct point), 0) == -1)
		{
			printf("NO SE RECIBIERON DATOS DEL SERVIDOR \n");
			stop = 1;

		}
		else
		{

			fflush(NULL);
			{

				id = last.id;
				last_point[id].id = id;
				prev_point[id].point[0] = last_point[id].point[0] ;
				prev_point[id].point[1] = last_point[id].point[1];
				if(last.point[0] == RELEASE)
				{
					draw_other[id] = 0;
				}	
				else if(last.point[0] == NEWCOLOR)
				{
					array_miembro[id].red = last.red;
					array_miembro[id].blue = last.blue;
					array_miembro[id].green = last.green;
					array_miembro[id].line_width = last.line_width;
					//printf("Color recibido: (%i, %i, %i)\n",array_miembro[id].red,array_miembro[id].green,array_miembro[id].blue);

					actualizar_pen(id);
					//printf("Miembro %i cambio de color\n",id);	

				}
				else if(last.point[0] == NEWFONDO)
				{
					gdk_threads_enter();
					cambiar_color_fondo(last.red,last.green,last.blue);
					gdk_threads_leave();
					color_blanco[0] = last.red;
					color_blanco[1] = last.green;
					color_blanco[2] = last.blue;
					if(borrar_clicked == 1){
						set_line_color(color_blanco[0],color_blanco[1],color_blanco[2]);
						enviar_borrar();
					}


				}
				else
				{
					//printf("IMPRIMIR %i %i\n",last.point[0],last.point[1]);
					last_point[id].point[0] = last.point[0];
					last_point[id].point[1] = last.point[1];
					gdk_threads_enter();
					draw_brush_other(id);
					gdk_threads_leave();
				}
			}		
			//printf("Miembro %i dibuja\n",id);
		}
		
	}
	conectado = 0;
	pthread_exit(0);

}

//evento que se ejecuta al presionar el boton conectar
//establece la coneccion al servidor y no se puede volver a usar hasta que haya una desconeccion
static gint boton_conectar_clicked(GtkWidget *widget, GdkEvent *event)
{
	if(ini_coneccion==0)
	{
		ini_coneccion = 1;

		//guardan los ultimos 2 puntos de cada miembro para dibujar una recta
	    last_point = (struct point*)malloc(sizeof(struct point)*nt);
	    prev_point = (struct point*)malloc(sizeof(struct point)*nt);

	    //dos hilos para escuchar por colores y por puntos
	    //tid = malloc(sizeof(pthread_t)*2);

	    //arreglo de crayones
	    array_pen = (GdkGC **) malloc(sizeof(GdkGC *)*nt);

	    array_miembro = (struct point*)malloc(sizeof(struct point)*nt);

	    //creo el arreglo para los colores de los miembros
	    array_colors = (GdkColor **) malloc(sizeof(GdkColor *)*nt);

	    int i;
	    for(i=0;i<nt;i++)
	    {
	    	draw_other[i] = 0;
	    	//creo crayones para los demas miembros
	    	array_pen[i] = GetPen(pixmap,0,0,0, &array_colors[i]);
	    	set_line_width(5,array_pen[i]);
	    }
	}
	if(conectado == 0)
	{
		int status;
		stop = 0;
		conectado = 1;
		if ((he = gethostbyname(dir)) == NULL) 
		{
			printf("ERROR EN EL NOMBRE O DIRECCION INGRESADA %s NO ES UN NOMBRE VALIDO\n", dir);
			conectado = 0;
			return 1;
		}

		//se crea el socket y se checkea que la creacion sea exitosa
		if ((sockfd_point = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
		{
			printf("ERROR EN LA CREACION DEL SOCKET\n");
			conectado = 0;
			return 1;
		}

			addr.sin_family = AF_INET;
			addr.sin_port = htons(PORT);

			addr.sin_addr = *((struct in_addr *)he->h_addr); 
			bzero(&(addr.sin_zero), 8); 


		if (connect(sockfd_point, (struct sockaddr*) &addr, sizeof(struct sockaddr)) == -1) 
		{
			printf("NO SE PUDO CONECTAR CON EL SERVIDOR\n");
			conectado = 0;
			return 1;
		}
		else
		{
			
			//creando hilo para escuchar al servidor
			status = pthread_create(&tid, NULL, escuchar_por_puntos, (void*) 0);
			//en caso de error
			if(status)
			      conectado = 0;
			//enviando el color actual al servidor
			
			printf("Coneccion establecida\n");
/**/	}
	}
}

//evento que controla el tamaño de la linea a partir del spin de la interfaz
static gint spin_pressed(GtkWidget *widget, GdkEvent *event)
{
	int value = gtk_spin_button_get_value_as_int((GtkSpinButton*)widget);
	
	line_width = value;

	set_line_width(value,pen);

	if(borrar_clicked == 0)
	    enviar_color();
	else
		enviar_borrar();
}

//evento que cambia el color de fondo borrando todo lo que estaba dibujado
static gint borrar_todo(GtkWidget *widget, GdkEvent *event)
{
	GdkColor *c = (GdkColor *) g_malloc (sizeof (GdkColor));

    gtk_color_button_get_color((GtkColorButton*)widget, c);
    gdk_color_alloc (gdk_colormap_get_system (), c);

    enviar_color_fondo(c);

}

//finaliza las conecciones con el servidor avisando primero que se va a desconetar
cerrar_conecciones()
{
	mi_punto.point[0] = DESCONECTAR;

	if (send(sockfd_point, &mi_punto, sizeof(struct point), 0) == -1)
		printf("Error la comunicarse con el servidor\n");
	stop = 1;
	close(sockfd_point);

}

//el evento de cerrar la ventana se utiliza para finalizar correctamente la ejecucion al presionar el boton de cerrar
static gint cerrar(GtkWidget *widget, GdkEvent *event)
{
	if(sockfd_point != -1){
		stop = 1;
		cerrar_conecciones();
	}
	gtk_main_quit();
}

//configura la gui a partir del archvio xml interfaz.glade
void config_gui()
{
	b = gtk_builder_new();
    gtk_builder_add_from_file(b, "interfaz.glade", NULL);
    FrmPrincipal = GTK_WIDGET(gtk_builder_get_object(b, "window1"));
    BtnColor = GTK_WIDGET(gtk_builder_get_object(b, "colorbutton1"));    
    BtnColorFondo = GTK_WIDGET(gtk_builder_get_object(b, "colorbutton-fondo"));    
	drawing_area = GTK_WIDGET(gtk_builder_get_object(b, "drawingarea1"));
	BtnBorrar = GTK_WIDGET(gtk_builder_get_object(b, "borrar"));
	BtnConectar = GTK_WIDGET(gtk_builder_get_object(b, "conectar"));
	BtnSpin = GTK_WIDGET(gtk_builder_get_object(b, "spinbutton1"));
	

    g_signal_connect(FrmPrincipal, "destroy", G_CALLBACK(cerrar), NULL);
   // g_signal_connect(FrmPrincipal, "destroy", G_CALLBACK(cerrar), NULL);

	gtk_signal_connect(GTK_OBJECT(drawing_area), "expose_event",(GtkSignalFunc) expose_event, NULL);
 	gtk_signal_connect(GTK_OBJECT(drawing_area),"configure_event",(GtkSignalFunc) configure_event, NULL);
  	gtk_signal_connect(GTK_OBJECT(drawing_area), "motion_notify_event",G_CALLBACK(motion_notify_event), NULL);
 	gtk_signal_connect(GTK_OBJECT(drawing_area), "button_press_event",G_CALLBACK(button_press_event), NULL);
 	gtk_signal_connect(GTK_OBJECT(drawing_area), "button_release_event",G_CALLBACK(button_release_event), NULL);

 	gtk_signal_connect(GTK_OBJECT(BtnColor), "color_set",G_CALLBACK(color_change_event), NULL);

 	gtk_signal_connect(GTK_OBJECT(BtnColorFondo), "color_set",G_CALLBACK(borrar_todo), NULL);


 	gtk_signal_connect(GTK_OBJECT(BtnBorrar), "clicked",G_CALLBACK(boton_borrar_clicked), NULL);

 	gtk_signal_connect(GTK_OBJECT(BtnConectar), "clicked",G_CALLBACK(boton_conectar_clicked), NULL);

 	gtk_signal_connect(GTK_OBJECT(BtnSpin), "value_changed",G_CALLBACK(spin_pressed), NULL);



  	gtk_widget_set_events (drawing_area, GDK_EXPOSURE_MASK
                         | GDK_LEAVE_NOTIFY_MASK
                         | GDK_BUTTON_PRESS_MASK
						 | GDK_BUTTON_RELEASE_MASK
                         | GDK_POINTER_MOTION_MASK
                         | GDK_POINTER_MOTION_HINT_MASK);


	g_object_unref(G_OBJECT (b));

}

void signal_handler(int sig){
	cerrar(NULL,NULL);
}

int main (int argc, char *argv[])
{
	if(argc<2){
		printf("ERROR EN LOS ARGUMENTOS\n");
		exit(1);
	}

	if(signal(SIGINT,signal_handler)==SIG_ERR){
		printf("Error en el manejador de ctrl+c\n");
		exit(1);
	}

	dir = argv[1];
	gdk_threads_init ();
	gdk_threads_enter();

    gtk_init (&argc, &argv);
    config_gui();




    gtk_widget_show_all(FrmPrincipal);                
    gtk_main();
    gdk_threads_leave();
    return 0;

}

