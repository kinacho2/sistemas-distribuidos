#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>
#include "connect.h"


#define BACKLOG 10



int nro_cliente = 0;

int* sockets_member;

struct point* last_color;

int* color_fondo[3]= {0xffff,0xffff,0xffff};

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void funcion_hilo_point(int id)
{
	struct point last;
	int i = 0;
	int tipo;
	int stop = 0;
	char c;

	last_color[id].id = id;


	for(i=0; i<nt; i++)
	{
				
		if(i!=id && sockets_member[i]!=-1)

			//printf("Enviando color del miembro %i al miembro %i: (%i, %i, %i)\n",i,id,last_color[i].red,last_color[i].green,last_color[i].blue);
			if (send(sockets_member[id], &last_color[i], sizeof(struct point), 0) == -1)
				perror("send");
				/**/
	}
	last.point[0] = NEWFONDO;
	last.red = color_fondo[0];
	last.green = color_fondo[1];
	last.blue = color_fondo[2];
	for(i=0; i<nt; i++)
	{
		
		if(sockets_member[i]!=-1)
		{
			if (send(sockets_member[i], &last, sizeof(struct point), 0) == -1)
				perror("send");
		}
		/**/
	}

	while(stop == 0)
	{
		
		{

			if (recv(sockets_member[id], &last, sizeof(struct point), 0) == -1)
			{
				printf("FUNCION PUNTO: NO SE RECIBIERON DATOS DEL CLIENTE %i\n", id);
				stop = 1;

			}
			else
			{
				fflush(NULL);

				if(last.point[0] == DESCONECTAR)
				{
					stop = 1;

				}
				else
				{
					last.id = id;
					//printf("Se recibio un nuevo punto del cliente %i: [%i,%i]\n",id,last.point[0],last.point[1]);
					if(last.point[0] == NEWCOLOR)
					{
						last_color[id].point[0] = NEWCOLOR;
						last_color[id].red = last.red;
						last_color[id].green = last.green;
						last_color[id].blue = last.blue;
						last_color[id].line_width = last.line_width;
					}
					if(last.point[0] == NEWFONDO)
					{
						color_fondo[0] = last.red;
						color_fondo[1] = last.green;
						color_fondo[2] = last.blue;

					}
					{ 
						pthread_mutex_lock(&mutex);
						//envio el valor del punto a todos los miembros
						for(i=0; i<nt; i++)
						{
							
							if(sockets_member[i]!=-1 && i!=id)
							{
								if (send(sockets_member[i], &last, sizeof(struct point), 0) == -1)
									perror("send");
							}
							/**/
						}
						pthread_mutex_unlock(&mutex);
					}
				}
			}
		}
		
	
	}

	printf("El cliente %i abandono la sala\n",id);
	close(sockets_member[id]);
	sockets_member[id] = -1; //marco

	//lock
	nro_cliente--;
	//unlock
	pthread_exit(0);

}

int next_cliente()
{
	int i = 0;
	while(sockets_member[i]!=-1 && i<nt)
		i++;
	if(i<BACKLOG)
		return i;
	else 
		return -1;
}

/**/
int main()
{
	int sockfd, newfd;
	int sin_size;
	int i = 0;
	int status;

	int actual;

	struct sockaddr_in clnt_addr;

	struct sockaddr_in svr_addr;
	
	sockets_member = (int*) malloc(sizeof(int)*nt);//por ahora maximo 10

	last_color = (struct point*) malloc(sizeof(struct point)*nt);//por ahora maximo 10

	pthread_mutex_unlock(&mutex);
	for(i=0;i<nt;i++)
	{
		sockets_member[i] = -1;
		bzero(last_color,sizeof(struct point)*nt);
	}

	pthread_t* tid = malloc(sizeof(pthread_t)*nt);

	

	//se crea el socket y se checkea
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	{
		printf("ERROR EN LA CREACION DEL SOCKET\n");
		exit(1);
	}
	
	//defino familia
	svr_addr.sin_family = AF_INET;
	//asigno puerto
	svr_addr.sin_port = htons(PORT);
	//se asigna la ip local
	svr_addr.sin_addr.s_addr = INADDR_ANY;
	//rellena con ceros
	bzero(&(svr_addr.sin_zero), 8);


	//se ejecuta y checkea el enlazador (bind)
	if ( bind(sockfd, (struct sockaddr *)&svr_addr, sizeof(struct sockaddr)) == -1) 
	{
		printf("NO SE PUDO ENLAZAR EL SOKET\n");
		exit(1);
	}
	if (listen(sockfd, BACKLOG) != -1) 	
	//se ejecuta el listen
		while(1)//bucle 
		{
		if ((newfd = accept(sockfd, (struct sockaddr *)&clnt_addr, &sin_size)) != -1)
		if((actual = next_cliente())>-1)
		{
			//actual = next_cliente();
			sockets_member[actual] = newfd;
			char* ip = inet_ntoa_my(clnt_addr.sin_addr);
			printf("Se establecio una coneccion desde %s, ID miembro: %i\n",ip,actual);
			//envio todos los colores de los miembros actuales

			//se crean el hilo para recibir puntos
			status = pthread_create(&tid[actual], NULL, funcion_hilo_point, (void*) actual);
			//en caso de error
			if(status)
			      exit(-1);



			//si todo sale bien se incrementa el numero de cliente
		  	

		}
		else
		{
			close(newfd);
			printf("ERROR AL ACEPTAR LA CONECCION, MAXIMA CANTIDAD DE MIEMBROS\n");
		}
		
		
					
		}//fin del bucle
	else
	{
			printf("ERROR EN LA FUNCION LISTEN\n");
	}


	return 0;
} 
