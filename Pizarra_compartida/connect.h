#include <gtk/gtk.h>

#define PORT 14555
#define nt 10


#define RELEASE -2000
#define NEWCOLOR -3000
#define NEWFONDO -4000
#define DESCONECTAR -10000

char * 
inet_ntoa_my(in)
	struct in_addr in;
{
	static char b[18];
	register char *p;

	p = (char *)&in;
	#define	UC(b)	(((int)b)&0xff)
	(void)snprintf(b, sizeof(b),
	    "%d.%d.%d.%d%c", UC(p[0]), UC(p[1]), UC(p[2]), UC(p[3]),'\0');
	return (b);
}


struct point
{
	int id;
	int point[2];
	int line_width;
	gint red;
	gint green;
	gint blue;
};
